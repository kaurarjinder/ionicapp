import { Component, OnInit } from '@angular/core';
import { TodoService } from '../News/todo.service';
import { NewsItem } from '../News/news';


@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
  providers : [ TodoService]
})

export class Tab2Page {

// tslint:disable-next-line: variable-name

  newsArray: NewsItem[];
  // public items: Array<{ author: string; title: string;  }> = [];
  // totalresults: number;

  constructor( private todoservice: TodoService ) {   }

// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {    this.getList();   }

  getList(): void {

    this.todoservice.getList().subscribe(
      data => {
          this.newsArray = data.articles;
      },
      error => {
        console.error(error);
      }
   );
   

    

  }
}