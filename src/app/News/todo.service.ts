import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewsItem } from './news';
import { Observable } from 'rxjs';

const API_KEY = '5f60d66bdfac4ad5b1de83eef27d3a57';
const BASE_URL = 'https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=' + API_KEY;

@Injectable()
export class TodoService {

   // tslint:disable-next-line: variable-name
  _http: HttpClient;
  newsArray: NewsItem[];


  constructor( private http: HttpClient ) {  this._http = http;  }

// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {    this.getList();   }

  getList() {

        return this._http.get<any>(BASE_URL);





    }


  
}
