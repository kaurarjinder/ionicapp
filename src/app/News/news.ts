import { Component } from '@angular/core';

export class Source {
    id: string; 
    name: string;
}

export class NewsItem {

    source: Source;
    author: string;
    title: string;
    description: string;
    url: string;
    urlToImage: string;
    publishedAt: string;
    content: string;
}
