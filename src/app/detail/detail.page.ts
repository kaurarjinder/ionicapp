import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsItem } from '../News/news';
import { TodoService } from '../News/todo.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
  providers: [ TodoService]
})
export class DetailPage {



  newsid: string;
  newsitem: NewsItem[];
  id: number;
  flag = 0;

  constructor(private route: ActivatedRoute, private todoservice: TodoService) {
  }

  // Ionic tab pages are initialized in the constructor only once.
  // ionViewWillEnter() is called every time the tab page is viewed.
  ionViewWillEnter() {
    this.newsid = this.route.snapshot.paramMap.get('id');
    this.id = Number(this.newsid);
    this.getNews(this.id);
  }

  getNews(Id: number) {
    
    this.todoservice.getList().subscribe(

      data => {
        this.newsitem = data.articles[Id];
        this.flag = 1;
        console.log(this.newsitem);
        

    },
    error => {
      console.error(error);

    }
    );




  }

}

