import { Component, OnInit } from '@angular/core';
import { TodoService } from '../News/todo.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
  providers : [ TodoService]
})

export class Tab3Page {

  todoList: string[] = [];


  constructor(private storage: Storage) {
      this.readfromLocalStorage();
  }


// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {      }

  addTodo(todo: string) {
    this.todoList.push(todo);
    this.saveToStorage();
  }

  deleteTodo(index) {
    this.todoList.splice(index, 1);
    this.saveToStorage();
  }

  saveToStorage(){
    let jsonString = JSON.stringify(this.todoList);
    this.storage.set('todos', jsonString);
  }

  readfromLocalStorage(){
    this.storage.get('todos').then((val) => {
      this.todoList = JSON.parse(val);
    });

  }

  doReorder(ev: any){
    console.log('Dragged from index', ev.detail.from, 'to', ev.detail.to);
    let from = ev.detail.from;
    let to = ev.detail.to;

    var temp = this.todoList[from];
    this.todoList.splice(from, 1);

    this.todoList.splice(to, 0, temp);;

    this.saveToStorage();
    ev.detail.complete();

  }





}

